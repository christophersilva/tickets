<!DOCTYPE html>
<html>
<head>
  <title>Tickets</title>
  <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap-theme.min.css">
</head>
<body>
  <div class="col-md-12">
    <h2 class="page-header">Pré-configurações</h2>
    <form method="POST">
    	<div class="form-group">
        <label>Após quantos dias sem resolução os tickets se tornam de prioridade alta? </label>
        <input class="form-control" type="number" min="1" name="" value="20" id="days_to_high_priority">
      </div>
      <div class="form-group">
        <label>Quantos tickets aparecerão por página? </label>
        <input class="form-control" type="number" min="0" name="" value="15" id="per_page">
      </div>
      <div class="form-group">
        <label>Palavras negativas: </label>
        <textarea class="form-control" id="negative_words"></textarea>
        <span class="help-block">Algumas palavras estão configuradas para ajudar a analisar o humor do consumidor e o conteúdo, se quiser adicionar mais palavras, as adicione no campo separando com ponto e vírgula. Ex: Procon; ReclameAqui;</span>
        <span class="help-block">
          Palavras padrão: <br>
          Insatisfeito, Irritado, Insatisfeita, Irritada,
          Insatisfatório, Reclamação, Reclamar, Não,
          Ruim, Defeito, Problema, Procon, ReclameAqui,
        </span>
      </div>
      <input class="btn btn-default" type="submit" id="save" value="SALVAR">
    </form>
  </div>
  <script type="text/javascript" src="assets/javascript/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="assets/javascript/notify.min.js"></script>
  <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="assets/javascript/config.js"></script>
</body>
</html>