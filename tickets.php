<?php
  date_default_timezone_set('UTC');
?>
<!DOCTYPE html>
<html>
<head>
  <title>Tickets</title>
  <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap-theme.min.css"></head>
<body>
  <div class="col-md-12">
    <style type="text/css">
      .filter-form{
        display: flex;
        justify-content: space-between;
      }

      .action{
        margin-top: 25px;
        flex-wrap: wrap;
      }

      #total-tickets{
        margin: 20px 0;
        text-align: right;
      }
    </style>
    <h2 class="page-header">Tickets</h2>
    <form class="filter-form row">
      <div class="form-group col-md-2">
        <label>Prioridade</label>
        <select class="form-control" id="priority">
          <option value="ALL">Todas</option>
          <option value="ALTA">Alta</option>
          <option value="NORMAL">Normal</option>
        </select>
      </div>
      <div class="form-group col-md-3">
        <label>Data de criação Inicial</label>
        <input class="form-control" type="date" id="initialDate">
      </div>
      <div class="form-group col-md-3">
        <label>Data de criação Final</label>
        <input class="form-control" type="date" id="finalDate" value="<?php echo date('Y-m-d'); ?>"  max="<?php echo date('Y-m-d'); ?>">
      </div>
      <div class="form-group col-md-2">
        <label>Ordenar</label>
        <select id="order" class="form-control">
          <option value="Priority,DESC">Prioridade (Z-A)</option>
          <option value="Priority,ASC">Prioridade (A-Z)</option>
          <option value="DateCreate,DESC">Data de Criação (Z-A)</option>
          <option value="DateCreate,ASC">Data de Criação (A-Z)</option>
          <option value="DateUpdate,DESC">Data de Atualização (Z-A)</option>
          <option value="DateUpdate,ASC">Data de Atualização (A-Z)</option>
        </select>
      </div>
      <div class="action col-md-2">
        <input class="btn btn-default" type="submit" id="search" value="Filtrar">
      </div>
    </form>
    <table class="table table-bordered table-stripped">
      <thead>
        <tr>
          <th>TicketID</th>
          <th>CategoryID</th>
          <th>CustomerID</th>
          <th>CustomerName</th>
          <th>CustomerEmail</th>
          <th>DateCreate</th>
          <th>DateUpdate</th>
          <th>Prioridade</th>
        </tr>
      </thead>
      <tbody id="tickets-body">
      </tbody>
    </table>
    <div class="info-pagination row">
      <nav aria-label="Page navigation" class="col-md-6">
        <ul class="pagination">
        </ul>
      </nav>
      <div class="col-md-6" id="total-tickets">
      </div>
    </div>
  </div>
  <script type="text/javascript" src="assets/javascript/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="assets/javascript/tickets.js"></script>
</body>
</html>