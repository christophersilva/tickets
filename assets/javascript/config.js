$(window).on("load", function(){
  $.ajax({
    url: 'config/api_key.json',
    async: false,
    dataType: 'json',
    success: function (json) {   
      window.API_KEY = json.api_key;
    }
  });

  var filter_negative_words = function(){
    var negative_words = $("#negative_words").val().split(';');

    if(negative_words.length > 0){
      var filtered = negative_words.map(function(word){ return word.trim(); });
      filtered = filtered.filter(function(word){ return word != ''; });
      return filtered;
    }else{
      return [];
    }
  }

  var create_tickets_api_file = function(){
    $.ajax({
      url: 'api/tickets.php',
      type: 'POST',
      headers: { 'x-api-key': API_KEY },
      statusCode: {
        204: function(data) {
          window.location = '/tickets.php';
        },
        400: function(data) {
          $.notify(response.message, "error");
        },
        520: function(data) {
          $.notify(response.message, "error");
        },
      }
    });
  }

  $(document).on("click", "#save", function(){
    event.preventDefault();
    var negative_words = filter_negative_words();
    
    var params = {
      days_to_high_priority: $("#days_to_high_priority").val() || 20,
      per_page: $("#per_page").val() || 15,
      negative_words: negative_words
    };

    $.ajax({
      url: 'api/config.php',
      type: 'POST',
      data: params,
      headers: { 'x-api-key': API_KEY },
      statusCode: {
        201: function(data) {
          var message = JSON.parse(data).message;
          $.notify(message, "success");
          create_tickets_api_file();
        },
        400: function(data) {
          var response = JSON.parse(data.responseText);
          $.notify(response.message, "error");
        },
        500: function(data) {
          var response = JSON.parse(data.responseText);
          $.notify(response.message, "error");
        },
        520: function(data) {
          var response = JSON.parse(data.responseText);
          $.notify(response.message, "error");
        },
      }
    });
  });
});