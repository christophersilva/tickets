$(window).on("load", function(){
  $.ajax({
    url: 'config/api_key.json',
    async: false,
    dataType: 'json',
    success: function (json) {   
      window.API_KEY = json.api_key;
    }
  });

  var format_date = function(date){
    var day  = date.getDate().toString(),
        dayF = (day.length == 1) ? '0'+day : day,
        month  = (date.getMonth()+1).toString(), //+1 pois no getMonth Janeiro começa com zero.
        monthF = (month.length == 1) ? '0'+month : month,
        yearF = date.getFullYear();
    return dayF+"/"+monthF+"/"+yearF;
  };

  var populate_tickets = function(tickets){
    var rows = '';
    $("#tickets-body").empty();
    tickets.map(function(ticket){
      var create = new Date(ticket.DateCreate),
          update = new Date(ticket.DateUpdate);
      rows += '<tr>\
        <td>' + ticket.TicketID + '</td>\
        <td>' + ticket.CategoryID + '</td>\
        <td>' + ticket.CustomerID + '</td>\
        <td>' + ticket.CustomerName + '</td>\
        <td>' + ticket.CustomerEmail + '</td>\
        <td>' + format_date(create) + ' ' + (create.toLocaleTimeString()) + '</td>\
        <td>' + format_date(update) + ' ' + (update.toLocaleTimeString()) + '</td>\
        <td class="bg-' + (ticket.Priority == "ALTA" ? "danger" : "warning") + '">' + ticket.Priority + '</td>\
      </tr>';
    });
    $("#tickets-body").append(rows);
  };

  var set_pages = function(page, total_pages){
    $(".pagination").empty();
    var pages = '';
    for (var i = 1; i <= parseInt(total_pages); i++){
      pages += '<li ' + (page == i ? 'class="active current-page"' : '') + ' data-page="' + i + '"><a href="#">' + i + '</a></li>';
    }

    $(".pagination").append(pages);
  };

  var set_total_tickets = function(total_tickets){
    $("#total-tickets").empty();
    $("#total-tickets").append(total_tickets + ' tickets encontrados');
  };

  var get_tickets = function(params = {}){
    event.preventDefault();
    $.ajax({
      url: 'api/tickets.php',
      async: false,
      type: 'GET',
      headers: { 'x-api-key': API_KEY },
      data: params,
      statusCode: {
        200: function(data) {
          var response = JSON.parse(data);
          populate_tickets(response.tickets);
          set_pages(response.page, response.total_pages);
          set_total_tickets(response.total_tickets);
        },
        400: function(data){
          $.notify(response.message, "error");
        }
      }
    });
  };

  $(document).on("click", "#search", function(){
    var params = {
      priority: $("#priority").val(),
      initial_date: $("#initialDate").val(),
      final_date: $("#finalDate").val(),
      page: $(".current-page").data("page"),
      order_by: $("#order").val()
    };
    get_tickets(params);
  });

  $(document).on("click", ".pagination li", function(){
    var page = $(this).data("page");
    var params = {
      priority: $("#priority").val(),
      initial_date: $("#initialDate").val(),
      final_date: $("#finalDate").val(),
      page: page,
      order_by: $("#order").val()
    };
    get_tickets(params);
  });

  get_tickets();
});