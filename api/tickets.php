<?php
  $api_key = json_decode(file_get_contents('config/api_key.json'))->api_key;
  $headers = getallheaders();

  if($headers['x-api-key'] === $api_key){
    require_once 'models/Ticket.class.php';
 
    if($_SERVER['REQUEST_METHOD'] === 'GET'){
      $per_page = filter_input(INPUT_GET, 'per_page');
      $page = filter_input(INPUT_GET, 'page') ? $_GET['page'] : 1;
      $order_by = filter_input(INPUT_GET, 'order_by');
      $filters = [
        'priority' => filter_input(INPUT_GET, 'priority') ? $_GET['priority'] : '',
        'initial_date' => filter_input(INPUT_GET, 'initial_date') ? $_GET['initial_date'] : '',
        'final_date' => filter_input(INPUT_GET, 'final_date') ? $_GET['final_date'] : ''
      ];

      $ticket = new Ticket($per_page);
      $ticket->filter($filters);
      $order_by = $ticket->validate_order($order_by);
      $ticket->order_by($order_by);
      $response = [
        'tickets' => $ticket->get_page($page),
        'total_pages' => $ticket->total_pages(),
        'total_tickets' => $ticket->total_tickets(),
        'per_page' => $ticket->__get('per_page'),
        'page' => $page
      ];
      http_response_code(200);
      echo json_encode($response);
    }else{
      $ticket = new Ticket();
      if($ticket->create_tickets_api_file() && $ticket->classify()){
        http_response_code(204);
      }else{
        http_response_code(520);
        echo json_encode('Ocorreu um erro desconhecido');
      }
    }
  }else{
    http_response_code(400);
    echo json_encode(['message' => 'Chave de API inválida']);
  }