<?php
  /*
  * This class is used to manipulate tickets
  */
  class Ticket{
    private $tickets;
    private $per_page;
    private $base_pontuation = 2;
    private $negative_words;
    private $days_to_high_priority;

    function __construct($per_page = 15){
      $this->tickets = json_decode(file_get_contents('tickets.json'));
      $configs = json_decode(file_get_contents('config/tickets_config.json'));
      $this->per_page = (is_numeric($per_page) && (int) $per_page > 0) ? $per_page : $configs->per_page;
      $this->days_to_high_priority = $configs->days_to_high_priority;
      $this->negative_words = $configs->negative_words;
    }

    /*
    * Method to get all loaded tickets
    */
    public function all(){
      return $this->__get('tickets');
    }

    /*
    * Method to get all tickets priority and save on json
    */
    public function classify(){
      $tickets = json_decode(file_get_contents('tickets.json'), TRUE);
      $total = count($tickets);
      for($x = 0; $x < $total; $x++) {
        $priority = $this->priority($tickets[$x]['Interactions']);
        $tickets[$x]['Priority'] = $priority['priority'];
        $tickets[$x]['Pontuation'] = $priority['pontuation'];
      }

      $create = file_put_contents(
        'tickets.json',
        json_encode(
          $tickets,
          JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE
        )
      );

      return $create;
    }

    /*
    * Method to copy tickets json from docs
    * @return bool
    */
    public function create_tickets_api_file(){
      $tickets_path = getcwd();
      $filename = '/tickets.json';
      $original_tickets_path = dirname($tickets_path).'/docs/'.$filename;
      return copy($original_tickets_path, $tickets_path.$filename);
    }

    /*
    * Method to filter tickets
    */
    public function filter($filters){
      date_default_timezone_set('UTC');
      if (($filters['priority'] == 'ALL' || $filters['priority'] == '') && $filters['initial_date'] == '' && $filters['final_date'] == '') return '';
      $tickets = $this->__get('tickets');
      if($filters['priority'] != 'ALL'){
        $tickets = array_filter($tickets, function($ticket) use(&$filters){
          return $ticket->Priority == $filters['priority'];
        });
      }

      $filters['initial_date'] = strtotime($filters['initial_date']);
      $filters['final_date'] = strtotime($filters['final_date']);

      $tickets = array_filter($tickets, function($ticket) use(&$filters){
        $date_create = strtotime($ticket->DateCreate);
        return ($filters['initial_date'] <= $date_create) && ($date_create <= $filters['final_date']);
      });

      $this->__set('tickets', $tickets);
    }

    /*
    * Method to get all tickets from passed page number
    * @return Array with tickets
    */
    public function get_page($page){
      $page = $page ? $page : 1;
      $per_page = $this->__get('per_page');
      $tickets = array_slice($this->__get('tickets'), (($page - 1) * $per_page), $per_page);
      return $tickets;
    }

    /*
    * Method to sort loaded tickets
    * @param string order is field and order type
    */
    public function order_by($order = 'Priority,DESC'){
      $tickets = $this->__get('tickets');
      list($field, $type) = explode(',', $order);
      if($field == 'Priority'){
        if ($type == 'DESC'){
          usort($tickets,function($first,$second) use(&$field){
            if (strtolower($first->$field) == strtolower($second->$field)) {
              return $second->Pontuation - $first->Pontuation;
            }
            return strtolower($first->$field) > strtolower($second->$field);
          });
        }else{
          usort($tickets,function($first,$second) use(&$field){
            if (strtolower($first->$field) == strtolower($second->$field)) {
              return $first->Pontuation - $second->Pontuation;
            }
            return strtolower($first->$field) < strtolower($second->$field);
          });
        }
      }elseif ($type == 'DESC'){
        usort($tickets,function($first,$second) use(&$field){
          return strtolower($first->$field) < strtolower($second->$field);
        });
      }else{
        usort($tickets,function($first,$second) use(&$field){
          return strtolower($first->$field) > strtolower($second->$field);
        });
      }
      $this->__set('tickets', $tickets);
    }

    /*
    * Method to check ticket priority based on interactions
    * @return Array with ticket priority and pontuation
    */    
    public function priority($interactions){
      $last_interaction = end($interactions);
      $pontuation = $this->base_pontuation;
      $priority = 'NORMAL';
      foreach ($interactions as $interaction) {
        if($interaction['Sender'] == 'Expert') continue;
        $subject = explode(' ', $interaction['Subject']);
        foreach ($subject as $word) {
          if($word == 'RE:') continue;
          if(ctype_upper($word)) $pontuation += 0.5;
          if(in_array(mb_strtolower($word), $this->negative_words)){
            $pontuation **= 2;
            $priority = 'ALTA';
          }
        }

        $message = explode(' ', $interaction['Message']);
        foreach ($message as $word) {
          if($word == 'RE:') continue;
          if(ctype_upper($word)) $pontuation += 0.5;
          if(in_array(mb_strtolower($word), $this->negative_words)){
            $pontuation **= 2;
            $priority = 'ALTA';
          }
        }
      }

      if ($last_interaction['Sender'] == 'Customer'){
        date_default_timezone_set('UTC'); 
        $start = date_create($interactions[0]['DateCreate']);
        $today = date_create();
        $diff = date_diff($start, $today);
        $pontuation += $diff->days;
        if($diff->days > 20) $priority = 'ALTA';
      }

      return ['priority' => $priority, 'pontuation' => ceil($pontuation)];
    }

    /*
    * Method to return total page number using config per page
    * @return int
    */
    public function total_pages(){
      $per_page = $this->__get('per_page');
      return ceil($this->total_tickets() / $per_page);
    }

    /*
    * Method to count all loaded tickets
    * @return int
    */
    public function total_tickets(){
      return count($this->__get('tickets'));
    }

    /*
    * Method to return all valid orders
    * @return Array
    */
    public function valid_orders(){
      return [
        'DateCreate,ASC',
        'DateCreate,DESC',
        'DateUpdate,ASC',
        'DateUpdate,DESC',
        'Priority,ASC',
        'Priority,DESC'
      ];
    }
    /*
    * Method to validate an order
    * @param string order
    * @return string order or fallback if passed order is invalid
    */
    public function validate_order($order){
      if(in_array($order, $this->valid_orders())) return $order;
      return 'Priority,DESC';
    }

    /*
    * Magic setter method that set an attribute
    */
    public function __get($attribute){
      return $this->$attribute;
    }

    /*
    * Magic getter method that return an attribute
    */
    public function __set($attribute, $value){
      $this->$attribute = $value;
    }
  }