<?php
  /*
  *  This class is used to save base configurations to Ticket class use.
  */
  class Config{

    private $per_page;
    private $days_to_high_priority;
    private $negative_words;
    private $default_negative_words = [
        'insatisfeito', 'irritado',
        'insatisfeita', 'irritada',
        'insatisfatório', 'insatisfatorio',
        'reclamação', 'reclamacao', 'reclamar',
        'não', 'nao', 'ruim', 'defeito',
        'problema', 'procon', 'reclameaqui'
      ];

      function __construct($per_page, $days_to_high_priority, $negative_words){
        $this->per_page = $per_page ? $per_page : 15;
        $this->days_to_high_priority = $days_to_high_priority ? $days_to_high_priority : 20;
        $this->negative_words = $negative_words ? $negative_words : [];
      }

      /*
    * Method to save all configs in tickets_config file that will be used in Ticket class
    * @return int $create This is the file size
      */
      public function create(){
        $configs = $this->get_full_configs();
          
          $create = file_put_contents(
            'config/tickets_config.json',
            json_encode(
              $configs,
              JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE
            )
          );

          return $create;
      }

      /*
    * Method that get all class configs and return an associative array
    * @return Array with all configs with associative keys
      */
      public function get_full_configs(){
        return [
          'per_page' => $this->__get('per_page'),
          'days_to_high_priority' => $this->__get('days_to_high_priority'),
          'negative_words' => array_merge($this->__get('default_negative_words'), $this->__get('negative_words'))
        ];
      }

      /*
    * Magic getter method that return an attribute
      */
      public function __get($attribute){
        return $this->$attribute;
      }
  }