<?php
	$api_key = json_decode(file_get_contents('config/api_key.json'))->api_key;

	$headers = getallheaders();

	if($headers['x-api-key'] === $api_key){
		$per_page = filter_input(INPUT_POST, 'per_page');
    $days_to_high_priority = filter_input(INPUT_POST, 'days_to_high_priority');
    $negative_words = filter_input(INPUT_POST, 'negative_words', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    require_once 'models/Config.class.php';
    $config = new Config($per_page, $days_to_high_priority, $negative_words);
  
    if($config->create()){
      http_response_code(201);
      $msg = 'Configurações salvas';
    }else{
      http_response_code(520);
      $msg = 'Ocorreu um erro desconhecido';
    }
  }else{
    http_response_code(400);
    $msg = 'Chave de API inválida';
  }

  echo json_encode(['message' => $msg]);
